﻿using BibliotecaAppRouss.Clases;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;

namespace BibliotecaAppRouss.Catalogos
{
    class CatalogoVotacion : CatalogoGenerico<Votacion>
    {
        public static List<Votacion> RecuperarVigentesConParticipantes(ISession nhSesion)
        {
            try
            {
                List<Votacion> listaVotaciones = nhSesion.Query<Votacion>().Where(x => x.FechaHoraDesde < DateTime.Now && x.FechaHoraHasta > DateTime.Now && x.ParticipantesVotacion.Count > 0).ToList();
                return listaVotaciones;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
