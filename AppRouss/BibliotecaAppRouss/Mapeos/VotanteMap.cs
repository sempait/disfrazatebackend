﻿using BibliotecaAppRouss.Clases;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotecaAppRouss.Mapeos
{
    public class VotanteMap : ClassMap<Votante>
    {
        public VotanteMap()
        {
            Table("Votantes");
            Id(x => x.Codigo).Column("codigoVotante").GeneratedBy.Identity();
            Map(x => x.FechaHoraVoto).Column("fechaHoraVoto");

            References(x => x.Usuario).Column("codigoUsuario").Cascade.None().LazyLoad(Laziness.Proxy);
        }
    }
}
