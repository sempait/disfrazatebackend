﻿using BibliotecaAppRouss.Clases;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotecaAppRouss.Mapeos
{
    public class VotacionMap : ClassMap<Votacion>
    {
        public VotacionMap()
        {
            Table("Votaciones");
            Id(x => x.Codigo).Column("codigoVotacion").GeneratedBy.Identity();
            Map(x => x.Descripcion).Column("descripcion");
            Map(x => x.FechaHoraDesde).Column("fechaHoraDesde");
            Map(x => x.FechaHoraHasta).Column("fechaHoraHasta");

            HasMany<ParticipanteVotacion>(x => x.ParticipantesVotacion).KeyColumn("codigoVotacion").Not.KeyNullable().Cascade.AllDeleteOrphan().LazyLoad();
        }
    }
}
