﻿using BibliotecaAppRouss.Clases;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotecaAppRouss.Mapeos
{
    public class ParticipanteVotacionMap : ClassMap<ParticipanteVotacion>
    {
        public ParticipanteVotacionMap()
        {
            Table("ParticipantesVotacion");
            Id(x => x.Codigo).Column("codigoParticipante").GeneratedBy.Identity();
            Map(x => x.Descripcion).Column("descripcion");
            Map(x => x.RutaImagen).Column("rutaFoto");

            HasMany<Votante>(x => x.Votantes).KeyColumn("codigoParticipante").Not.KeyNullable().Cascade.AllDeleteOrphan().LazyLoad();
        }
    }
}
