﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotecaAppRouss.Clases
{
    public class ParticipanteVotacion
    {
        public ParticipanteVotacion()
        {
            Votantes = new List<Votante>();
        }

        public virtual int Codigo { get; set; }
        public virtual string Descripcion { get; set; }
        public virtual string RutaImagen { get; set; }

        public virtual IList<Votante> Votantes { get; set; }
    }
}
