﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotecaAppRouss.Clases
{
    public class Votacion
    {
        public Votacion()
        {
            ParticipantesVotacion = new List<ParticipanteVotacion>();
        }

        public virtual int Codigo { get; set; }
        public virtual string Descripcion { get; set; }
        public virtual DateTime FechaHoraDesde { get; set; }
        public virtual DateTime? FechaHoraHasta { get; set; }

        public virtual IList<ParticipanteVotacion> ParticipantesVotacion { get; set; }
    }
}
