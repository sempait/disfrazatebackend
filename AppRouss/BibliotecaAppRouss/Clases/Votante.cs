﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotecaAppRouss.Clases
{
    public class Votante
    {
        public virtual int Codigo { get; set; }
        public virtual DateTime FechaHoraVoto { get; set; }

        public virtual Usuario Usuario { get; set; }
    }
}
