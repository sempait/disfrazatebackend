﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BibliotecaAppRouss.Controladores;

namespace AppRouss
{
    public partial class postulacionAdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadDddlVotaciones();
            }
        }

        private void LoadDddlVotaciones()
        {
            DataTable dtSorteos = ControladorGeneral.RecuperarTodasVotaciones();
            ddlVotacion.DataSource = dtSorteos;
            ddlVotacion.DataTextField = "descripcion";
            ddlVotacion.DataValueField = "codigoVotacion";
            ddlVotacion.DataBind();
            ddlVotacion.Items.Insert(0, new ListItem("--Seleccione una votacion--", "0"));
            ddlVotacion.SelectedIndex = ddlVotacion.Items.IndexOf(ddlVotacion.Items.FindByText("--Seleccione un sorteo--"));
        }

        protected void btnConfirmar_Click(object sender, EventArgs e)
        {
            if (validar())
            {
                ControladorGeneral.InsertarActualizarParticipanteVotacion(0, txtDescripcion.Text, Session["rutaImagen"].ToString(), int.Parse(ddlVotacion.SelectedValue));
                lblMensajeSorteo.Text = "La postulación se ha creado correctamente.";
                pcPublicidad.ShowOnPageLoad = true;
            }
        }

        private bool validar()
        {
            if (txtDescripcion.Text == "" || string.IsNullOrWhiteSpace(txtDescripcion.Text) == true || string.IsNullOrEmpty(txtDescripcion.Text) == true)
            { lblDescripcion.Visible = true; lblDescripcion.InnerText = " El campo es requerido"; return false; }

            
            else if (Session["rutaImagen"] == null)
            {
                if (fuImagen.HasFile && (fuImagen.PostedFile.ContentType == "image/jpg" || fuImagen.PostedFile.ContentType == "image/jpeg"))
                {
                    //if (fuImagen.Width == 2048 && fuImagen.Height == 1536)
                    //{
                    string path = Server.MapPath(".") + "\\assets\\2015\\" + fuImagen.FileName;
                    fuImagen.PostedFile.SaveAs(path);
                    StreamReader reader = new StreamReader(fuImagen.FileContent);
                    string text = reader.ReadToEnd();
                    Session.Add("rutaImagen", fuImagen.FileName);
                    //}
                    //else { lblImagen.InnerText = "Debe cargar una imagen con la siguiente resolución.  "; lblImagen.Visible = true; return false; }
                }
                else { lblImagen.InnerText = "Debe cargar una imagen valida. Por ejemplo: .jpg, .jpeg"; lblDescripcion.Visible = false; lblImagen.Visible = true; return false; }
            }
            return true;
        }



        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("postulacion.aspx");
        }

        protected void btnAceptarMensaje_Click(object sender, EventArgs e)
        {
            pcPublicidad.ShowOnPageLoad = false;
            Response.Redirect("postulacion.aspx");
        }
    }
}