﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BibliotecaAppRouss.Controladores;

namespace AppRouss
{
    public partial class votacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadGridVotaciones();
        }

        private void LoadGridVotaciones()
        {
            gvVotacion.DataSource = ControladorGeneral.RecuperarTodasVotaciones();
            gvVotacion.DataBind();
        }

        protected void btnNewConcurso_Click(object sender, EventArgs e)
        {
            Response.Redirect("votacionAdd.aspx");
        }

        protected void btnEliminarConcurso_Click(object sender, EventArgs e)
        {
            DataTable dtParticipantesPorSorteo = ControladorGeneral.RecuperarVotantesPorVotacion(obtenerCodigoFilaSeleccionada());

            if (dtParticipantesPorSorteo.Rows.Count != 0)
            {
                lblMensajeEliminarSorteo.Text = "Adevertencia, La votacion que quiere elimina posee usuario que ya han participado. Precione ACEPTAR si desea continuar y eliminar todos los juegos del mismo o precione CANCELAR para volver.";
            }
            else
            {
                lblMensajeEliminarSorteo.Text = "¿Esta seguro que desea eliminar la votacion?";
            }
            pcPublicidad.ShowOnPageLoad = true;
        }

        private int obtenerCodigoFilaSeleccionada()
        {
            int codigo = 0;
            codigo = int.Parse(gvVotacion.GetRowValues(gvVotacion.FocusedRowIndex, "codigoVotacion").ToString());
            return codigo;
        }

        protected void btnConfirmar_Click(object sender, EventArgs e)
        {
            ControladorGeneral.EliminarVotacion(obtenerCodigoFilaSeleccionada());
            pcPublicidad.ShowOnPageLoad = false;
            Response.Redirect("votacion.aspx");
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            pcPublicidad.ShowOnPageLoad = false;
        }

    }
}