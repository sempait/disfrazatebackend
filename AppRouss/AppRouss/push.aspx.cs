﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BibliotecaAppRouss.Controladores;
using BibliotecaAppRouss.Clases;
using System.Data;

namespace AppRouss
{
    public partial class push : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadGridPush();
            }
        }

        private void LoadGridPush()
        {
            gvPush.DataSource = ControladorGeneral.RecuperarTodasPush(false); //se agrega booleano
            gvPush.DataBind();
        }

        protected void btnNewPush_Click(object sender, EventArgs e)
        {
            pcPush.ShowOnPageLoad = true;
        }

        protected void btnConfirmar_Click(object sender, EventArgs e)
        {
            enviarPush();
        }

        private void enviarPush()
        {
            if (validar())
            {
                Push pushActual = new Push();
                pushActual.Descripcion = txtPush.Text;
                pushActual.FechaHoraEnvio = DateTime.Now;

                ControladorGeneral.InsertarActualizarPush(0, txtPush.Text, DateTime.Now, false); //cambiar isAutomatica
                BibliotecaAppRouss.ClasesComplementarias.PushNotification.Enviar(txtPush.Text);

                lblDescripcionPush.Visible = false;
                txtPush.Visible = false;
                btnCancelar.Visible = false;
                btnConfirmar.Visible = false;
                lblPush.Visible = false;
                lblInformacionPush.Visible = true;
                lblInformacionPush.InnerText = "El mensaje se ha enviado correctamente.";

                LoadGridPush();
            }
            else
            {
                pcPush.ShowOnPageLoad = false;
            }
        }

        private bool validar()
        {
            if (txtPush.Text == "" || string.IsNullOrWhiteSpace(txtPush.Text) == true || string.IsNullOrEmpty(txtPush.Text) == true)
            { lblPush.InnerText = " El campo es requerido"; return false; }

            return true;
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            pcPush.ShowOnPageLoad = false;
        }


        protected void btnPushAutomatica_Click(object sender, EventArgs e)
        {
            DataTable dtMensajeActual = ControladorGeneral.RecuperarTodasPush(true);
            txtMensajeAutomatico.Text = dtMensajeActual.Rows[0]["descripcion"].ToString();
            pcMensajeAutomatico.ShowOnPageLoad = true;
        }

        protected void btnCancelarMensajeAutomatico_Click(object sender, EventArgs e)
        {
            pcMensajeAutomatico.ShowOnPageLoad = false;
        }

        protected void btnConfirmarMensajeAutomatico_Click(object sender, EventArgs e)
        {
            CargarMensajeAutomatico();
        }

        private void CargarMensajeAutomatico()
        {
            if (validar2())
            {
                ControladorGeneral.InsertarActualizarPush(0, txtMensajeAutomatico.Text, DateTime.Now, true); //cambiar isAutomatica
                pcMensajeAutomatico.ShowOnPageLoad = false;
            }
            else
            {
                pcPush.ShowOnPageLoad = false;
            }
        }

        private bool validar2()
        {
            if (txtMensajeAutomatico.Text == "" || string.IsNullOrWhiteSpace(txtMensajeAutomatico.Text) == true || string.IsNullOrEmpty(txtMensajeAutomatico.Text) == true)
                { Label2.InnerText = " El campo es requerido"; return false; }
            else return true;
        }

    }
}