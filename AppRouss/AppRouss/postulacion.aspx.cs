﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BibliotecaAppRouss.Controladores;

namespace AppRouss
{
    public partial class postulacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["rutaImagen"] = null;
            if (!IsPostBack)
            {
                LoadDDLVotacion();
            }
        }

        private void LoadDDLVotacion()
        {
            DataTable dtSorteos = ControladorGeneral.RecuperarTodasVotaciones();
            ddlVotacion.DataSource = dtSorteos;
            ddlVotacion.DataTextField = "descripcion";
            ddlVotacion.DataValueField = "codigoVotacion";
            ddlVotacion.DataBind();
            ddlVotacion.Items.Insert(0, new ListItem("--Seleccione una votacion--", "0"));
            ddlVotacion.SelectedIndex = ddlVotacion.Items.IndexOf(ddlVotacion.Items.FindByText("--Seleccione una Votacion--"));
        }

        private void LoadGridPostulaciones()
        {
            gvPostulacion.DataSource = ControladorGeneral.RecuperarTodosParticipantesVotacion(int.Parse(ddlVotacion.SelectedValue));
            gvPostulacion.DataBind();
        }

        protected void btnNewPostulacion_Click(object sender, EventArgs e)
        {
            Response.Redirect("postulacionAdd.aspx");
        }

        protected void btnEliminarPostulacion_Click(object sender, EventArgs e)
        {
            lblMensajeEliminarPostulacion.Text = "¿Esta seguro que desea eliminar la postulacion?";

            pcPublicidad.ShowOnPageLoad = true;
        }

        protected void btnConfirmar_Click(object sender, EventArgs e)
        {
            ControladorGeneral.EliminarParticipanteVotacion(obtenerCodigoFilaSeleccionada());
            pcPublicidad.ShowOnPageLoad = false;
            Response.Redirect("postulacion.aspx");
        }


        private int obtenerCodigoFilaSeleccionada()
        {
            int codigo = 0;
            codigo = int.Parse(gvPostulacion.GetRowValues(gvPostulacion.FocusedRowIndex, "codigoParticipante").ToString());
            return codigo;
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            pcPublicidad.ShowOnPageLoad = false;
        }

        protected void ddlVotacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadGridPostulaciones();
        }

    }
}