﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BibliotecaAppRouss.Clases;
using BibliotecaAppRouss.Controladores;

namespace AppRouss
{
    public partial class votacionAdd : System.Web.UI.Page
    {
        Votacion oVotacionActual;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                deFechaDesde.Date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, int.Parse("00"), int.Parse("00"), int.Parse("00"));
                deFechaHasta.Date = deFechaDesde.Date.AddDays(1);
            }
        }

        protected void btnConfirmar_Click(object sender, EventArgs e)
        {
            if (validar())
            {
                ControladorGeneral.InsertarActualizarVotacion(0, txtDescripcionConcurso.Text, DateTime.Parse(deFechaDesde.Text), DateTime.Parse(deFechaHasta.Text));
                lblMensajeConcurso.Text = "La votación se ha creado correctamente.";
                pcSorteos.ShowOnPageLoad = true;
            }
        }

        private bool validar()
        {
            if (txtDescripcionConcurso.Text == "" || string.IsNullOrWhiteSpace(txtDescripcionConcurso.Text) == true || string.IsNullOrEmpty(txtDescripcionConcurso.Text) == true)
            { lblDescripcion.InnerText = " El campo es requerido"; lblDescripcion.Visible = true; lblFechaDesde.Visible = false; return false; }

            if (Convert.ToString(deFechaDesde.Date) == "01/01/0001 0:00:00" || deFechaDesde.Date == null)
            { lblFechaDesde.InnerText = " El campo es requerido"; lblFechaDesde.Visible = true; lblFechaHasta.Visible = false; return false; }
            if (Convert.ToString(deFechaHasta.Date) == "01/01/0001 0:00:00" || deFechaHasta.Date == null)
            { lblFechaHasta.InnerText = " El campo es requerido"; lblFechaHasta.Visible = true; lblFechaDesde.Visible = false; return false; }
            if (DateTime.Compare(deFechaDesde.Date, deFechaHasta.Date) >= 0)
            { lblFechaDesde.InnerText = " La Fecha Desde debe ser menor que la Fecha Fin del sorteo."; lblFechaDesde.Visible = true; lblFechaHasta.Visible = false; return false; }

            //else if (!validarFechas2())
            //{ lblFechaDesde.InnerText = " El rango de Fecha se corresponde con las fechas de un sorte existente. Pruebe con otra fecha. "; lblFechaDesde.Visible = true; lblFechaHasta.Visible = false; lblCantidadVictorias.Visible = false; lblCantidadOportunidades.Visible = false; lblCantidadTotalPremios.Visible = false; return false; }
            else return true;
        }

        private bool validarFechas2()
        {
            DataTable dtSorteos = ControladorGeneral.RecuperarTodasVotaciones();
            for (int i = 0; i < dtSorteos.Rows.Count; i++)
            {
                DateTime iFechaDesde = Convert.ToDateTime(dtSorteos.Rows[i]["fechaDesde"]);
                DateTime iFechaHasta = Convert.ToDateTime(dtSorteos.Rows[i]["fechaHasta"]);
                if (deFechaDesde.Date < iFechaDesde && deFechaHasta.Date > iFechaHasta)
                    return false;

                if ((iFechaDesde <= deFechaDesde.Date && deFechaDesde.Date <= iFechaHasta) || (iFechaDesde <= deFechaHasta.Date && deFechaHasta.Date <= iFechaHasta))
                {
                    //return false;
                    if (Session["codigoOperacion"] == null)
                    {
                        oVotacionActual = (Votacion)Session["sorteoActual"];
                        if (oVotacionActual.Codigo == Convert.ToInt32(dtSorteos.Rows[i]["codigoSorteo"]))
                            return true;
                    }
                }

                if ((iFechaDesde <= deFechaDesde.Date && deFechaDesde.Date <= iFechaHasta) || (iFechaDesde <= deFechaHasta.Date && deFechaHasta.Date <= iFechaHasta))
                {
                    return false;
                }
            }
            return true;
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("votacion.aspx");
        }

        protected void btnAceptarMensaje_Click(object sender, EventArgs e)
        {
            pcSorteos.ShowOnPageLoad = false;
            Response.Redirect("votacion.aspx");
        }
    }
}