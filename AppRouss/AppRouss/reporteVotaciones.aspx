﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="reporteVotaciones.aspx.cs" Inherits="AppRouss.reporteVotaciones" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>


<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">Reporte Votaciones <small>reportes & estadisticas</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.aspx">Inicio</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Reportes</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="reporteVotaciones.aspx">Reporte Votaciones</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">

                    <!-- BEGIN ROW -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN CHART PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bar-chart font-green-haze"></i>
                                        <span class="caption-subject bold uppercase font-green-haze">Reporte de Votaciones</span>
                                        <span class="caption-helper">Listado de Votaciones.</span>
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="javascript:;" class="fullscreen"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <!-- BEGIN PAGE CONTENT-->
                                <div class="portlet light">
                                    <div class="portlet-body">
                                        <!-- Meer Our Team -->
                                        <div class="headline">
                                            <h3>Meet Our Team</h3>
                                        </div>
                                        <div class="row thumbnails">
                                            <div class="col-md-3">
                                                <div class="meet-our-team">
                                                    <h3>Bob Nilson <small>Chief Executive Officer / CEO</small></h3>
                                                    <dx:ASPxImage ID="img1" runat="server" class="img-responsive"></dx:ASPxImage>
                                                    <div class="team-info">
                                                        <p>
                                                            Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...
                                                        </p>
                                                        <ul class="social-icons pull-right">
                                                            <li>
                                                                <a href="javascript:;" data-original-title="twitter" class="twitter"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="facebook" class="facebook"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="linkedin" class="linkedin"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="Goole Plus" class="googleplus"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="skype" class="skype"></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="meet-our-team">
                                                    <h3>Marta Doe <small>Project Manager</small></h3>
                                                    <img src="../../assets/admin/pages/media/pages/3.jpg" alt="" class="img-responsive" />
                                                    <div class="team-info">
                                                        <p>
                                                            Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...
                                                        </p>
                                                        <ul class="social-icons pull-right">
                                                            <li>
                                                                <a href="javascript:;" data-original-title="twitter" class="twitter"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="facebook" class="facebook"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="linkedin" class="linkedin"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="Goole Plus" class="googleplus"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="skype" class="skype"></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="meet-our-team">
                                                    <h3>Bob Nilson <small>Chief Executive Officer / CEO</small></h3>
                                                    <img src="../../assets/admin/pages/media/pages/2.jpg" alt="" class="img-responsive" />
                                                    <div class="team-info">
                                                        <p>
                                                            Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...
                                                        </p>
                                                        <ul class="social-icons pull-right">
                                                            <li>
                                                                <a href="javascript:;" data-original-title="twitter" class="twitter"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="facebook" class="facebook"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="linkedin" class="linkedin"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="Goole Plus" class="googleplus"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="skype" class="skype"></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="meet-our-team">
                                                    <h3>Marta Doe <small>Project Manager</small></h3>
                                                    <img src="../../assets/admin/pages/media/pages/3.jpg" alt="" class="img-responsive" />
                                                    <div class="team-info">
                                                        <p>
                                                            Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...
                                                        </p>
                                                        <ul class="social-icons pull-right">
                                                            <li>
                                                                <a href="javascript:;" data-original-title="twitter" class="twitter"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="facebook" class="facebook"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="linkedin" class="linkedin"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="Goole Plus" class="googleplus"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="skype" class="skype"></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/thumbnails-->
                                        <!-- //End Meer Our Team -->

                                        <!-- Meer Our Team -->
                                        <div class="headline">
                                            <h3>Meet Our Team</h3>
                                        </div>
                                        <div class="row thumbnails">
                                            <div class="col-md-3">
                                                <div class="meet-our-team">
                                                    <h3>Bob Nilson <small>Chief Executive Officer / CEO</small></h3>
                                                    <dx:ASPxImage ID="ASPxImage1" runat="server" class="img-responsive"></dx:ASPxImage>
                                                    <div class="team-info">
                                                        <p>
                                                            Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...
                                                        </p>
                                                        <ul class="social-icons pull-right">
                                                            <li>
                                                                <a href="javascript:;" data-original-title="twitter" class="twitter"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="facebook" class="facebook"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="linkedin" class="linkedin"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="Goole Plus" class="googleplus"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="skype" class="skype"></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="meet-our-team">
                                                    <h3>Marta Doe <small>Project Manager</small></h3>
                                                    <img src="../../assets/admin/pages/media/pages/3.jpg" alt="" class="img-responsive" />
                                                    <div class="team-info">
                                                        <p>
                                                            Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...
                                                        </p>
                                                        <ul class="social-icons pull-right">
                                                            <li>
                                                                <a href="javascript:;" data-original-title="twitter" class="twitter"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="facebook" class="facebook"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="linkedin" class="linkedin"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="Goole Plus" class="googleplus"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="skype" class="skype"></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="meet-our-team">
                                                    <h3>Bob Nilson <small>Chief Executive Officer / CEO</small></h3>
                                                    <img src="../../assets/admin/pages/media/pages/2.jpg" alt="" class="img-responsive" />
                                                    <div class="team-info">
                                                        <p>
                                                            Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...
                                                        </p>
                                                        <ul class="social-icons pull-right">
                                                            <li>
                                                                <a href="javascript:;" data-original-title="twitter" class="twitter"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="facebook" class="facebook"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="linkedin" class="linkedin"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="Goole Plus" class="googleplus"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="skype" class="skype"></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="meet-our-team">
                                                    <h3>Marta Doe <small>Project Manager</small></h3>
                                                    <img src="../../assets/admin/pages/media/pages/3.jpg" alt="" class="img-responsive" />
                                                    <div class="team-info">
                                                        <p>
                                                            Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...
                                                        </p>
                                                        <ul class="social-icons pull-right">
                                                            <li>
                                                                <a href="javascript:;" data-original-title="twitter" class="twitter"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="facebook" class="facebook"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="linkedin" class="linkedin"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="Goole Plus" class="googleplus"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" data-original-title="skype" class="skype"></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/thumbnails-->
                                        <!-- //End Meer Our Team -->

                                    </div>
                                </div>

                            </div>
                            <!-- END CHART PORTLET-->
                        </div>
                    </div>
                    <!-- END ROW -->

                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <!-- END CONTENT -->
    <!-- END FOOTER -->

    <script>
        jQuery(document).ready(function () {
            Metronic.init(); // init metronic core componets
            Layout.init(); // init layout
            QuickSidebar.init(); // init quick sidebar
            Demo.init(); // init demo features 
            Index.init();
            Index.initDashboardDaterange();
            Index.initJQVMAP(); // init index page's custom scripts
            Index.initCalendar(); // init index page's custom scripts
            Index.initCharts(); // init index page's custom scripts
            Index.initChat();
            Index.initMiniCharts();
            Tasks.initDashboardWidget();
        });
    </script>
    <!-- END JAVASCRIPTS -->
</asp:Content>

