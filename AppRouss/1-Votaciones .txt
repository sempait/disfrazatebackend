USE [AppRouss]
GO

/****** Object:  Table [dbo].[Votaciones]    Script Date: 14/08/2015 15:12:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Votaciones](
	[codigoVotacion] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](200) NOT NULL,
	[fechaHoraDesde] [datetime] NOT NULL,
	[fechaHoraHasta] [datetime] NULL,
 CONSTRAINT [PK_Votaciones] PRIMARY KEY CLUSTERED 
(
	[codigoVotacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


